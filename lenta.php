<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Memorial Website</title>
    <style>
        /* CSS for menu */
        nav {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 10px 0;
        }

        nav a {
            color: #fff;
            text-decoration: none;
            padding: 0 10px;
        }

        nav a:hover {
            text-decoration: underline;
        }

        /* CSS for content */
        .content {
            text-align: center;
            padding: 20px;
        }

        .content img {
            margin: 0 auto; /* Встановлюємо відступи по горизонталі для центрування */
            display: block; /* Прибираємо лишні відступи */
        }
        /* Стилі для заголовка */
        h1 {
            text-align: center;
        }
        /* Стилі для контейнера */
        .container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }
        /* Стилі для карточки обстрілу */
        .card {
            margin: 10px;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            width: 300px;
        }
        .card img {
            width: 100%;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <!-- Navigation Menu -->
    <nav>
        <a href="index.php">Головна</a>
        <a href="lenta.php">Лента</a>
        <a href="statics.php">Статистика</a>
        <a href="died.php">Надати нову інформацію</a>
    </nav>

    <!-- Контейнер для вставки динамічної інформації -->
    <div class="container" id="shootingsContainer"></div>

    <!-- JavaScript для взаємодії з сервером та відображення даних -->
    <script>
        // Коли сторінка завантажується, відправляємо запит на сервер для отримання даних про обстріли
        document.addEventListener("DOMContentLoaded", function() {
            // Використовуємо XMLHttpRequest для відправки запиту на сервер для отримання даних про обстріли
            var xhr = new XMLHttpRequest();//Цей об'єкт дозволяє виконувати запити на сервер без перезавантаження сторінки.
            xhr.open("GET", "get_shootings.php", true);//Встановлюється з'єднання з сервером.
            xhr.onload = function() {//Всередині цієї функції перевіряється статус запиту
                if (xhr.status == 200)
                 {
                    // Отримані дані про обстріли у форматі HTML
                    document.getElementById("shootingsContainer").innerHTML = xhr.responseText;
                }
            };
            xhr.send();//відправляємо фактичний запит на сервер
        });
    </script>
</body>
</html>
