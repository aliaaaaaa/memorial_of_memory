-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Бер 03 2024 р., 01:37
-- Версія сервера: 8.0.30
-- Версія PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `memorial`
--

-- --------------------------------------------------------

--
-- Дублююча структура для представлення `citymurders`
-- (Див. нижче для фактичного подання)
--
CREATE TABLE `citymurders` (
`City` varchar(255)
,`TotalMurders` bigint
);

-- --------------------------------------------------------

--
-- Структура таблиці `Shootings`
--

CREATE TABLE `Shootings` (
  `ShootingID` int NOT NULL,
  `Location` varchar(255) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `Description` text,
  `ImagePath` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп даних таблиці `Shootings`
--

INSERT INTO `Shootings` (`ShootingID`, `Location`, `Date`, `Description`, `ImagePath`) VALUES
(1, 'Київ', '2022-02-24', 'Артобстріл', '/img/shooting/image1.jpeg'),
(2, 'Харків', '2022-02-27', 'Ракетний удар', '/img/shooting/image2.jpg'),
(3, 'Одеса', '2022-03-01', 'Стрілянина', '/img/shooting/image3.jpg'),
(4, 'Харків', '2022-02-27', 'Ракетний удар', '/img/shooting/image2.jpg'),
(5, 'Одеса', '2022-03-01', 'Стрілянина', '/img/shooting/image3.jpg'),
(6, 'Львів', '2022-03-05', 'Бомбардування', '/img/shooting/image4.jpg'),
(7, 'Дніпро', '2022-03-10', 'Артобстріл', '/img/shooting/image5.jpg'),
(8, 'Херсон', '2022-03-15', 'Вибух', '/img/shooting/image6.jpg'),
(9, 'Запоріжжя', '2022-03-20', 'Підрив міни', '/img/shooting/image7.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `Victims`
--

CREATE TABLE `Victims` (
  `id` int NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `date_of_death` date DEFAULT NULL,
  `place_of_death` varchar(255) DEFAULT NULL,
  `description` text,
  `path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп даних таблиці `Victims`
--

INSERT INTO `Victims` (`id`, `first_name`, `last_name`, `date_of_death`, `place_of_death`, `description`, `path`) VALUES
(1, 'dfdfsds', 'fdsdsd', '2024-02-27', 'Київ', 'fdfdfsdfdsd', 'uploads/image1.jpeg'),
(2, 'dfdfsds', 'fdsdsd', '2024-02-27', 'Київ', 'fdfdfsdfdsd', 'uploads/image1.jpeg'),
(3, 'паав', 'ывывыв', '2024-03-04', 'Львів', 'пвава', 'uploads/image1.jpeg');

-- --------------------------------------------------------

--
-- Структура для представлення `citymurders`
--
DROP TABLE IF EXISTS `citymurders`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `citymurders`  AS SELECT `v`.`place_of_death` AS `City`, count(0) AS `TotalMurders` FROM (`victims` `v` join `shootings` `s` on((`v`.`place_of_death` = `s`.`Location`))) GROUP BY `v`.`place_of_death``place_of_death`  ;

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `Shootings`
--
ALTER TABLE `Shootings`
  ADD PRIMARY KEY (`ShootingID`);

--
-- Індекси таблиці `Victims`
--
ALTER TABLE `Victims`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `Shootings`
--
ALTER TABLE `Shootings`
  MODIFY `ShootingID` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблиці `Victims`
--
ALTER TABLE `Victims`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
