<!DOCTYPE html>
<html>
<head>
    <title>Інфографіка про вбивства</title>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head><style>
       nav {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 10px 0;
        }

        nav a {
            color: #fff;
            text-decoration: none;
            padding: 0 10px;
        }

        nav a:hover {
            text-decoration: underline;
        }

    </style>
<body>
<nav>
        <a href="index.php">Головна</a>
        <a href="lenta.php">Лента</a>
        <a href="statics.php">Статистика</a>
        <a href="died.php">Надати нову інформацію</a>
    </nav>
<h1>Інфографіка про вбивства у містах</h1>

<div id="plotly_chart"></div>

<?php
// Підключення до бази даних
require_once "database.php";
global $conn;
connectDB();

// SQL-запит для отримання даних
$sql = "SELECT * FROM CityMurders";
$result = $conn->query($sql);

// Створення масивів для даних графіка
$cities = array();
$murders = array();

// Обробка результатів запиту
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        array_push($cities, $row["City"]);
        array_push($murders, $row["TotalMurders"]);
    }
} else {
    echo "0 results";
}

// Закриття з'єднання з базою даних
$conn->close();

// Створення графіка
echo "<script>";
echo "var data = [{
        x: ['" . implode("', '", $cities) . "'],
        y: [" . implode(", ", $murders) . "],
        type: 'bar'
    }];
    Plotly.newPlot('plotly_chart', data);";
echo "</script>";

?>

</body>
</html>
