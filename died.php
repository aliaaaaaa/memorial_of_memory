<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Внесення інформації про загиблого</title>
</head>
<style>
       nav {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 10px 0;
        }

        nav a {
            color: #fff;
            text-decoration: none;
            padding: 0 10px;
        }

        nav a:hover {
            text-decoration: underline;
        }

    </style>
<body>
<nav>
        <a href="index.php">Головна</a>
        <a href="lenta.php">Лента</a>
        <a href="statics.php">Статистика</a>
        <a href="died.php">Надати нову інформацію</a>
    </nav>
    <h2>Внесення інформації про загиблого</h2>
    <form action="insert_victim.php" method="post" enctype="multipart/form-data">
        <label for="first_name">Ім'я:</label><br>
        <input type="text" id="first_name" name="first_name"><br>
        <label for="last_name">Прізвище:</label><br>
        <input type="text" id="last_name" name="last_name"><br>
        <label for="date_of_death">Дата смерті:</label><br>
        <input type="date" id="date_of_death" name="date_of_death"><br>
        <label for="place_of_death">Місце смерті:</label><br>
<select id="place_of_death" name="place_of_death">
    <option value="Київ">Київ</option>
    <option value="Харків">Харків</option>
    <option value="Одеса">Одеса</option>
    <option value="Дніпро">Дніпро</option>
    <option value="Донецьк">Донецьк</option>
    <option value="Запоріжжя">Запоріжжя</option>
    <option value="Львів">Львів</option>
    <option value="Кривий Ріг">Кривий Ріг</option>
    <option value="Миколаїв">Миколаїв</option>
    <option value="Маріуполь">Маріуполь</option>
    <option value="Вінниця">Вінниця</option>
    <option value="Херсон">Херсон</option>
    <option value="Полтава">Полтава</option>
    <option value="Черкаси">Черкаси</option>
    <option value="Житомир">Житомир</option>
    <option value="Чернігів">Чернігів</option>
    <option value="Чернівці">Чернівці</option>
    <option value="Суми">Суми</option>
    <option value="Рівне">Рівне</option>
    <option value="Кам'янець-Подільський">Кам'янець-Подільський</option>
    <option value="Івано-Франківськ">Івано-Франківськ</option>
    <option value="Кропивницький">Кропивницький</option>
    <option value="Тернопіль">Тернопіль</option>
    <option value="Луцьк">Луцьк</option>
    <option value="Ужгород">Ужгород</option>
    <option value="Севастополь">Севастополь</option>
    <option value="Бердянськ">Бердянськ</option>
    <option value="Хмельницький">Хмельницький</option>
    <option value="Івано-Франківськ">Івано-Франківськ</option>
</select><br>

        <label for="description">Опис:</label><br>
        <textarea id="description" name="description" rows="4" cols="50"></textarea><br>
        <label for="photo">Фотографія:</label><br>
        <input type="file" id="photo" name="photo"><br><br>
        <input type="submit" value="Додати загиблого">
    </form>
</body>
</html>
