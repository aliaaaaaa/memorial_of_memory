<?php
// Підключення до бази даних
require_once "database.php";
global $conn;
connectDB();
// Обробка і збереження введених даних
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$date_of_death = $_POST['date_of_death'];
$place_of_death = $_POST['place_of_death'];
$description = $_POST['description'];

// Збереження фотографії
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["photo"]["name"]);
move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file);

// SQL-запит для вставки даних
$sql = "INSERT INTO Victims (first_name, last_name, date_of_death, place_of_death, description, path)
VALUES ('$first_name', '$last_name', '$date_of_death', '$place_of_death', '$description', '$target_file')";

// Виконання запиту
if ($conn->query($sql) === TRUE) {
    echo "Інформація про загиблого успішно додана.";
    header("Location: index.php"); // Перенаправлення на index.php
    exit; // Важливо додати exit, щоб забезпечити коректну роботу перенаправлення
} else {
    echo "Помилка: " . $sql . "<br>" . $conn->error;
}

// Закриття з'єднання
$conn->close();
?>
