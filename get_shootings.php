<?php
// Підключення до бази даних
require_once "database.php";
global $conn;
connectDB();

// Виконання запиту до бази даних для отримання інформації з таблиці "Shootings"
$query = "SELECT * FROM Shootings";
$result = mysqli_query($conn, $query);

// Перевірка наявності результатів
if ($result) {
    // Цикл для перегляду кожного рядка результатів
    while ($row = mysqli_fetch_assoc($result)) {
        // Виведення інформації про кожен обстріл у форматі HTML
        echo "<div class='card'>";
        echo "<h2>{$row['Location']}</h2>";
        echo "<p>{$row['Date']}</p>";
        echo "<p>{$row['Description']}</p>";
        // Перевірка наявності шляху до зображення
        if (!empty($row['ImagePath'])) {
            echo "<img src='{$row['ImagePath']}' alt='Shooting Image'>";
        } else {
            echo "<p>Зображення відсутнє</p>";
        }
        echo "</div>";
    }
    // Звільнення результатів
    mysqli_free_result($result);
} else {
    // Якщо виникла помилка запиту
    echo "Помилка: " . mysqli_error($conn);
}
?>
