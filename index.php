<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Memorial Website</title>
    <style>
        /* CSS for menu */
        nav {
            background-color: #333;
            color: #fff;
            text-align: center;
            padding: 10px 0;
        }

        nav a {
            color: #fff;
            text-decoration: none;
            padding: 0 10px;
        }

        nav a:hover {
            text-decoration: underline;
        }

        /* CSS for content */
        .content {
            text-align: center;
            padding: 20px;
        }
      
        .content img {
            margin: 0 auto; /* Встановлюємо відступи по горизонталі для центрування */
            display: block; /* Прибираємо лишні відступи */
        }
 
    </style>
</head>
<body>
    <!-- Navigation Menu -->
    <nav>
        <a href="index.php">Головна</a>
        <a href="lenta.php">Лента</a>
        <a href="statics.php">Статистика</a>
        <a href="died.php">Надати нову інформацію</a>
    </nav> 

    <!-- Main Content -->
    <div class="content">
        <h1>Таймлайн російського вторгення в Україну</h1>
        <img src="https://upload.wikimedia.org/wikipedia/commons/7/72/2022_Russian_Invasion_of_Ukraine_animated.gif" alt="Timeline">

 
    </div>
</body>
</html>
